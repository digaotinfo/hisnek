# Hisnek

### Criar arquivo .env com base no .env-sample
Atualizar as chaves de acesso do DynamoDB
```
AWS_ACCESS_KEY_ID=***
AWS_SECRET_ACCESS_KEY=***
```

## Rodar os seguintes comandos para inicializar o projeto
```bash
npm install 
npm run dev
```
## Acesso ao Swagger 
```
http://localhost:3334//documentation
```

## Usar o routes.js para executar os endpoints