import { randomUUID } from "node:crypto";
import { db, tableUsers } from "../config/dbconfig.js";

import { Crypto } from '../utils/crypto.js';
const utilsCryto = new Crypto();
const paramsBaseDb = {TableName: tableUsers};

export class User {

    async created(data) {

        
        const {name, email} = data;
        try {
            
            if (!name || !email || !data.password) {
                throw 'Informe o name, email e password!';
            }
    
            const password = utilsCryto.encrypt(data.password);
            const id = randomUUID();
            const createdAt = new Date().toISOString()

            const newUser = {
                id, name, email, password, createdAt
            }
            
            await db.put({
                ...paramsBaseDb,
                Item: newUser
            }).promise();
            
            return {sucesso: true, data: newUser};
        } catch (error) {
            return {sucesso: false, message: error};
        }

    }
    
    async list(search) {

        try {

            let params = paramsBaseDb;

            if (search) {

                params = {...params,  
                    ExpressionAttributeNames: { "#status": "status" },
                    ExpressionAttributeValues: { ':search': search },
                    FilterExpression: "begins_with(#status, :search)",
                }
            }

            const {Items = []} = await db.scan(params).promise();
            
            return Array.from(Items).map((item) => {
                return {
                    ...item,
                    "password": item.password ? utilsCryto.getSenhaDecrypt(item.password) : null
                }
            })
            
        }
        catch (error) {
            console.log(error)
        }

    }

    async delete(id) {
        
        try {

            await db.delete({
                ...paramsBaseDb,
                Key: {
                    ['id']: id
                }
            }).promise();

            return {'sucesso': true};

        }
        catch (error) {
            return {'sucesso': false};
        }
    }

    async findEmailPsw(email, psw) {

        try {
            
            const {Items} = await db.scan({
                ...paramsBaseDb,  
                FilterExpression: "email=:email",
                ExpressionAttributeValues: { ':email': email },
            }).promise();
            
            if (!Items.length > 0) {
                return {};
            }

            Items[0].password = utilsCryto.getSenhaDecrypt(Items[0].password);
            if (Items[0].password !== psw) {
                throw 'Senha inválida.';
            }  
            return Items[0];

        }
        catch (error) {
            console.log('error', error);
            return {sucesso: false, message: error};
        }

    }

    async findByEmail(email) {

        try {
            
            const {Items} = await db.scan({
                ...paramsBaseDb,  
                FilterExpression: "email=:email",
                ExpressionAttributeValues: { ':email': email },
            }).promise();
            
            if (!Items.length > 0) {
                return {};
            }
                       
            return Items[0];

        }
        catch (error) {
            console.log('error', error);
            return {sucesso: false, message: error};
        }

    }

    async findById(id) {

        try {
            
            const { Item } = await db.get({
                ...paramsBaseDb,
                Key: { id: id }
            }).promise();
            // console.log(id)
            return Item;

        }
        catch (error) {
            console.log('error', error);
            return {sucesso: false, message: error};
        }

    }

    async update(id, user) {

        try {
            
            if (!id) {
                throw 'ID é campo obrigatório!';
            }
            
            const dbRegister = await this.findById(id);
            if (!dbRegister) {
                throw `Registro ${id} não encontrado!`;
            }
            user = {...dbRegister, ...user}
                        
            await db.put({
                ...paramsBaseDb,
                Item: user
            }).promise();
         
            return {sucesso: true};    
        }
        catch (error) {
            return {sucesso: false, message: error};
        }
    }
}