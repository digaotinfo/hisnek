import { fastify } from 'fastify';
import { awsLambdaFastify } from 'aws-lambda-fastify';
import { Tasks } from './database/Tasks.js';
import { User } from './database/User.js';
import { Jwt } from './utils/jwt.js';

const server = fastify();
const tashsDb = new Tasks();
const UserDatabase = new User();
const JWTToken = new Jwt();

import Swagger from '@fastify/swagger';
import SwaggerUI from '@fastify/swagger-ui';
import { swaggerConfig } from './utils/swaggerConfig.js';

await server.register(Swagger, swaggerConfig);
await server.register(SwaggerUI);
const proxy = awsLambdaFastify(server);

server.post('/users', {
    schema: {
        description: 'Novo usuário',
        tags: ['User'],
        summary: 'Criar novo Usuário',
        response: {
            200: {
                description: 'Sucesso Response',
                type: 'object',
            }
        },
        params: {
            type: 'object',
            properties: {
                name: {
                    type: 'string',
                    description: 'Nome de usuário'
                },
                email: {
                    type: 'string',
                    description: 'E-mail'
                },
                password: {
                    // required: false,
                    type: 'string',
                    description: 'Senha'
                }
            },
        }
    }
}, async(request, reply) => {
    
    // const { search } = request.query;
    const action = UserDatabase.created(request.body);
    if (action.sucesso === false) {
        return reply.status(404).send({
            'message': action.message
        });
    }
    return reply.status(201).send();
});

server.get('/users', {
    schema: {
        description: 'Listar usuário',
        tags: ['User'],
        summary: 'Listar todos os Usuários',
        response: {
            200: {
                description: 'Sucesso Response',
                type: 'array',
            }
        },
    }
}, async(request, reply) => {
    
    const { search } = request.query;
    return UserDatabase.list(search);
    
});

server.put('/users/:id', 
{
    preHandler: async (request, reply, done) => {
        await JWTToken.validarJwt(request, reply);
    },
    schema: {
        description: 'Alterar usuário',
        tags: ['User'],
        summary: 'Alterar Usuário informado',
        response: {
            200: {
                description: 'Sucesso Response',
                type: 'object',
            }
        },
        headers: {
            'x-access-token': {
                type: 'string',
                description: 'Token JWT fornecido no login'
            }
        },
        params: {
            type: 'object',
            properties: {
                name: {
                    type: 'string',
                    description: 'Nome de usuário'
                },
                email: {
                    type: 'string',
                    description: 'E-mail'
                },
                password: {
                    type: 'string',
                    description: 'Senha'
                }
            },
        },
    }
},
async (request, reply) => {
    
    const action = await UserDatabase.update(request.params.id, request.body);

    if (action.sucesso === false) {
        return reply.status(404).send({
            'message': action.message
        });
    }
    
    return reply.status(204).send();
});

server.delete('/users/:id', {
    preHandler: async (request, reply, done) => {
        await JWTToken.validarJwt(request, reply);
    },
    schema: {
        description: 'Exluir usuário',
        tags: ['User'],
        summary: 'Apagar Usuário informado',
        response: {
            200: {
                description: 'Sucesso Response',
                type: 'object',
            }
        },
        params: {
            type: 'object',
            properties: {
                id: {
                    type: 'string',
                    description: 'ID do Usuário'
                },
            }
        }
    }
}, async(request, reply) => {
    
    UserDatabase.delete(request.params.id);
    return reply.status(204).send();
    
});

server.post('/login', {
    schema: {
        description: 'Login',
        tags: ['JWT'],
        summary: 'Login para gerar JWT',
        
    }
}, async(request, reply) => {
    
    const { email, password } = request.body;
    const login = await UserDatabase.findEmailPsw(email, password);
    
    if (login.sucesso === false) {
        return reply.status(404).send({
            'message': login.message
        });
        
    }

    login.token = JWTToken.generateToken(login.id);
    login.auth = true;

    return reply.status(200).send(login);
    
});


server.post('/tasks', {
    preHandler: async (request, reply, done) => {
        await JWTToken.validarJwt(request, reply);
    },
    schema: {
        description: 'Criar nova Task',
        tags: ['Tasks'],
        summary: 'Nova Task',
        headers: {
            'x-access-token': {
                type: 'string',
                description: 'Token JWT fornecido no login'
            }
        },
        required: ['title', 'description'],
        params: {
            type: 'object',
            properties: {
                title: {
                    type: 'string',
                    description: 'Titulo'
                },
                description: {
                    type: 'string',
                    description: 'Descrição'
                },
                status: {
                    // required: false,
                    type: 'string',
                    description: 'peding|active'
                }
            },
        },
        response: {
            200: {
                description: 'Sucesso Response',
                type: 'object',
            }
        },
        body: {
            type: 'object',
            properties: {
                title: { type: 'string' },
                description: { type: 'string' },
                status: { type: 'string' }
            }
        }
    }
}, async (request, reply) => {

    const action = await tashsDb.createOrUpdate(request.body);

    if (action.sucesso === false) {
        return reply.status(404).send({
            'message': action.message
        });
    }
    return reply.status(201).send();
});

server.get('/tasks', {
    preHandler: async (request, reply, done) => {
        await JWTToken.validarJwt(request, reply);
    },
    schema: {
        description: 'Listar Tasks',
        tags: ['Tasks'],
        summary: 'Listar todas as Task',
        headers: {
            'x-access-token': {
                type: 'string',
                description: 'Token JWT fornecido no login'
            }
        },
        response: {
            200: {
                description: 'Sucesso Response',
                type: 'array',
            }
        }
    }
} , (request, reply) => {

    const { search } = request.query;
    return tashsDb.list(search);
});

server.put('/tasks/:id', {
    preHandler: async (request, reply, done) => {
        await JWTToken.validarJwt(request, reply);
    },
    schema: {
        description: 'Alterar Tasks',
        tags: ['Tasks'],
        summary: 'Atualizar a task',
        params: {
            type: 'object',
            properties: {
                title: {
                    type: 'string',
                    description: 'Titulo'
                },
                description: {
                    type: 'string',
                    description: 'Descrição'
                },
                status: {
                    // required: false,
                    type: 'string',
                    description: 'peding|active'
                }
            },
        },
        response: {
            200: {
                description: 'Sucesso Response',
                type: 'object',
            }
        },
        headers: {
            'x-access-token': {
                type: 'string',
                description: 'Token JWT fornecido no login'
            }
        },
        body: {
            type: 'object',
            properties: {
                title: { type: 'string' },
                description: { type: 'string' },
                status: { type: 'string' }
            }
        }
    }
}, async (request, reply) => {
    
    const action = await tashsDb.update(request.params.id, request.body);

    if (action.sucesso === false) {
        return reply.status(404).send({
            'message': action.message
        });
    }
    
    return reply.status(204).send();
});

server.delete('/tasks/:id', {
    preHandler: async (request, reply, done) => {
        await JWTToken.validarJwt(request, reply);
    },
    schema: {
        description: 'Excluir Tasks',
        tags: ['Tasks'],
        summary: 'Apagar a task',
        params: {
            type: 'object',
            properties: {
                id: {
                    type: 'string',
                    description: 'ID da Task'
                },
            }
        },
        response: {
            200: {
                description: 'Sucesso Response',
                type: 'object',
            }
        }
    }
}, (request, reply) => {
    
    tashsDb.delete(request.params.id);
    return reply.status(204).send();
});

server.listen({
    port: 3334
});
