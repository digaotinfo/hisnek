import { awsLambdaFastify } from 'aws-lambda-fastify';
import "./index.js";

const server = fastify({logger: true})

const proxy = awsLambdaFastify(server);
exports.handler = proxy; 