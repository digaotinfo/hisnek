import crypto from 'crypto';

const algoritmo = 'aes-256-ctr';
const secretKey = 'v6tsoLj97lD3vnwjQY5zvumFJ0jM8N8d';

export class Crypto {
    encrypt(text) {
        
        const iv = crypto.randomBytes(16);
        const cifra = crypto.createCipheriv(algoritmo, secretKey, iv);
        const encript = Buffer.concat([cifra.update(text), cifra.final()]);

        return iv.toString('hex') + '_' + encript.toString('hex');
    }

    decrypt = (hash) => {
        
        const decifrar = crypto.createDecipheriv(algoritmo, secretKey, Buffer.from(hash.iv, 'hex'));
        const decriptar = Buffer.concat([decifrar.update(Buffer.from(hash.content, 'hex')), decifrar.final()]);

        return decriptar.toString();
    }

    getSenhaDecrypt = (senhaEncrypt) => {
        const senhaSplit = senhaEncrypt.split('_');
        return this.decrypt({ iv: senhaSplit[0], content: senhaSplit[1] });
    }

}