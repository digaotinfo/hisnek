const swaggerConfig = {
    openapi: {
        info: {
            title: 'Hisnek',
            description: 'Projeto em node + DynamoDB',
            version: '1.0.0',
        },
        servers: {
            url: 'http://localhost:3334'
        }
    },
    hideUmtagged: true,
    expoeRoute: true
};

export {swaggerConfig}