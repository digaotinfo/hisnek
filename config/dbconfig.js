import 'dotenv/config'

import AWS from 'aws-sdk';

AWS.config.update({
    region: 'us-east-1',
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});

const db = new AWS.DynamoDB.DocumentClient();
const tableTasks = 'tasks';
const tableUsers = 'users';

export {
    db, tableTasks, tableUsers
}