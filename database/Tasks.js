import { randomUUID } from "node:crypto";
import { db, tableTasks } from "../config/dbconfig.js";

const paramsBaseDb = {TableName: tableTasks};

export class Tasks {

    async list(search) {

        try {

            let params = paramsBaseDb;

            if (search) {

                params = {...params,  
                    ExpressionAttributeNames: { "#status": "status" },
                    ExpressionAttributeValues: { ':search': search },
                    FilterExpression: "begins_with(#status, :search)",
                }
            }

            const {Items = []} = await db.scan(params).promise();
            
            return Items;

        }
        catch (error) {
            console.log(error)
        }

    }

    async findById(id) {

        try {

            const { Item } = await db.get({
                ...paramsBaseDb,
                Key: { id: id }
            }).promise();

            return Item
        }
        catch (error) {
            return {sucesso: false, message: error};
        }

    }

    async createOrUpdate(task) {

        try {

            const { id, title, description, status } = task;
            
            if (!id && !title) {
                throw 'Informe o titulo!';
            }

            if (!id && !description) {
                throw 'Informe uma descrição!';
            }

            if (!id && (!status || status === 'undefined')) {
                task = {...task, 'status': 'pending'}
            }
            
            if (id) {

                const dbRegister = await this.findById(id);
                if (!dbRegister) {
                    throw `Registro ${id} não encontrado!`;
                }
                task = {...dbRegister, ...task}

            }
            else {
                task = {...task, id: randomUUID(), createdAt: new Date().toISOString()};
            }
            
            await db.put({
                ...paramsBaseDb,
                Item: task
            }).promise();
            
            return {sucesso: true};
        }
        catch (error) {
            return {sucesso: false, message: error};
        }

    }

    async update(id, task) {

        try {

            if (!id) {
                throw 'ID é campo obrigatório!';
            }

            const action = await this.createOrUpdate({...task, id});
            if (action.sucesso === false) {
                return {sucesso: false, message: action.message};
            }

            return {sucesso: true};    
        }
        catch (error) {
            return {sucesso: false, message: error};
        }
        
    }

    async delete(id) {
        
        try {
            await db.delete({
                ...paramsBaseDb,
                Key: {
                    ['id']: id
                }
            }).promise();

            return {'sucesso': true};

        }
        catch (error) {
            return {'sucesso': false};
        }
    }

}