import jsonwebtoken from 'jsonwebtoken';
import { User } from '../database/User.js';

const SECRET = 'mysecret';
const TIME_EXPIRES_IN = "1d";
const UserDatabase = new User();

export class Jwt {
    
    generateToken(id) {
        return jsonwebtoken.sign({id}, SECRET, {expiresIn: TIME_EXPIRES_IN});
    }

    async validarJwt(request, reply) {
        
        try {
            
            const token = request.headers['x-access-token'];
            if (!token) {
                throw {code: 401, message: 'O token não foi fornecido!'};
            }
            
            const decoded = jsonwebtoken.verify(token, SECRET);
            
            const dataUserJwt = await UserDatabase.findById(decoded.id);
            if (!dataUserJwt) {
                throw {code: 500, message: 'Token não encontrado!'};
            }

            request.userJwt = dataUserJwt;

            return request;
        } catch (error) {
            
            let msg = error.message ? error.message : 'Falha ao validar token';
            let code = error.code ? error.code : 500;
            if(msg === "invalid signature"){
                msg = 'Token inválido!';
                code = 500;
            }
            return reply.status(code).send({auth: false, message: msg});
        }
        
    }
        
}